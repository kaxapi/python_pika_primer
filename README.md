# README #

A minimal primer of a durable queue configuration with a producer & competing consumers for RabbitMQ Python 3 application.
To install pika with pip3 execute the following command:

```
#!bash

pip3 install pika
```
