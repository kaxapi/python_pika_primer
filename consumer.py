
import pika
import json

# init rabbitmq queues
print("init rabbitmq...")
connection = pika.BlockingConnection(pika.ConnectionParameters(
            host='localhost', heartbeat_interval=300))
channel = connection.channel()
channel.basic_qos(prefetch_count=1)
q_name = 'rmq_test_queue'
channel.queue_declare(queue=q_name, durable=True)

def recv_data(ch, method, properties, body):
    print (len(body))
    user = json.loads(body.decode())
    print (user)
    ch.basic_ack(delivery_tag = method.delivery_tag)

channel.basic_qos(prefetch_count = 1)
channel.basic_consume(recv_data,
                      queue=q_name,
                      no_ack=False)

print ("consuming...")
channel.start_consuming()



