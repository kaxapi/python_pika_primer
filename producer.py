import pika
import json

# init rabbitmq queues
print("init rabbitmq...")
connection = pika.BlockingConnection(pika.ConnectionParameters(
            host='localhost', heartbeat_interval=300))
channel = connection.channel()
channel.basic_qos(prefetch_count=1)
q_name = 'rmq_test_queue'
channel.queue_declare(queue=q_name, durable=True)

def send(data):
    channel.basic_publish(exchange='', routing_key=q_name,
                      body=json.dumps(data,  ensure_ascii = False),
                      properties=pika.BasicProperties(
                      delivery_mode = 2,))

for i in range(100):
    a={'id': i, 'url': 'http://test.url%d.com' % i, 'status': 'available'}
    send(a)

